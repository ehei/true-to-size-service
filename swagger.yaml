swagger: '2.0'
info:
  description: This is an API for True to Size information.
  version: 1.0.0
  title: True to Size API
  contact:
    email: ericheikkila@gmail.com
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
tags:
  - name: general
    description: General URLs
  - name: shoe
    description: Operations on shoes
  - name: truesize
    description: Operations for true size
paths:
  /ping:
    get:
      tags:
        - general
      summary: simple heartbeat response
      operationId: ping
      responses:
        '200':
          description: "success"
  /status:
    get:
      tags:
        - general
      summary: simple status of the API
      operationId: status
      description: Returns "UP"
      produces:
        - application/json
      responses:
        '200':
          description: API status
          schema:
            $ref: '#/definitions/Status'
  /authenticate:
    post:
      tags:
        - security
      summary: authenticate a client
      operationId: authenticate
      description: Pass the correct client id and token will return a jwt token
      consumes:
        - "application/json"
        - "application/xml"
      produces:
        - "application/xml"
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "client information"
          required: true
          schema:
            $ref: "#/definitions/ClientInfo"
      responses:
        '200':
          description: successful authentication
          schema:
            $ref: '#/definitions/Token'
        '500':
          description: |
            missing or invalid client id or client token will only result
            in an unhelpful 500 error with no message in order to not allow
            the discovery of the correct values through brute force or other
            attacks
  /shoe:
    get:
      tags:
        - shoe
      summary: list of all shoes
      operationId: shoe
      description: Returns an array of all shoes in the true to size system
      produces:
        - application/json
      responses:
        '200':
          description: shoes
          schema:
            $ref: '#/definitions/Shoes'
      security:
        - Bearer: []
    post:
      tags:
        - shoe
      summary: "Add a new shoe entry"
      description: ""
      operationId: "addShoe"
      consumes:
        - "application/json"
        - "application/xml"
      produces:
        - "application/xml"
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "Name of shoe to add"
          required: true
          schema:
            $ref: "#/definitions/ShoeName"
      responses:
        406:
          description: "Key (name)=(NAME) already exists."
      security:
        - Bearer: []
  /shoe/{shoeId}:
    get:
      tags:
        - shoe
      summary: "Find shoe by ID"
      description: "Returns a single shoe"
      operationId: "getShoeById"
      produces:
        - "application/xml"
        - "application/json"
      parameters:
        - name: "shoeId"
          in: "path"
          description: "ID of shoe to return"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "successful operation"
          schema:
            $ref: "#/definitions/Shoe"
        400:
          description: "Invalid ID supplied"
        404:
          description: "Shoe not found"
      security:
        - Bearer: []
    post:
      tags:
        - shoe
      summary: "Add a new size entry"
      description: ""
      operationId: "addSize"
      consumes:
        - "application/json"
        - "application/xml"
      produces:
        - "application/xml"
        - "application/json"
      parameters:
        - name: "shoeId"
          in: "path"
          description: "ID of shoe to update"
          required: true
          type: "integer"
          format: "int64"
        - in: "body"
          name: "body"
          description: "Size to add"
          required: true
          schema:
            $ref: "#/definitions/ShoeSize"
      responses:
        200:
          description: "successful operation"
          schema:
            $ref: "#/definitions/CalculatedSize"
        404:
          description: "Shoe not found"
        500:
          description: "error"
      security:
        - Bearer: []
  /truesize/{shoeId}:
    get:
      tags:
        - truesize
      summary: "Find true size of shoe by ID"
      description: "Returns the true size of a single shoe"
      operationId: "getShoeSizeById"
      produces:
        - "application/xml"
        - "application/json"
      parameters:
        - name: "shoeId"
          in: "path"
          description: "ID of shoe to return"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "successful operation"
          schema:
            $ref: "#/definitions/TrueSizeData"
        404:
          description: "Shoe not found"
        500:
          description: "error"
      security:
        - Bearer: []
definitions:
  Status:
    type: object
    required:
      - status
    properties:
      status:
        type: string
        example: UP
  ShoeName:
    type: object
    required:
      - name
    properties:
      name:
        type: string
        example: ADIDAS wrapper
  Shoes:
    type: array
    items:
      $ref: '#/definitions/Shoe'
    example:
      - id: 12
        name: Reebok First
        size_calculation: 3
        size_data:
          - 2
          - 3
          - 4
      - id: 33
        name: Adidas Second
        size_calculation: 1
        size_data:
          - 3
          - 3
          - 3
  Shoe:
    type: object
    required:
      - id
      - name
      - size_calculation
      - size_data
    properties:
      id:
        type: integer
        example: 12
      name:
        type: string
        example: ADIDAS something
      size_calculation:
        type: number
        example: 2.34
      size_data:
        type: array
        items:
          type: number
          example: 2.3
  ShoeId:
    type: object
    required:
      - id
    properties:
      id:
        type: integer
        example: 345
  ShoeSize:
    type: object
    required:
      - size
    properties:
      size:
        type: integer
        example: 3
  CalculatedSize:
    type: object
    required:
      - size_calculation
    properties:
      size_calculation:
        type: number
        example: 2.533
  TrueSizeData:
    type: object
    required:
      - truesize
      - description
    properties:
      truesize:
        type: number
        example: 3.4
      description:
        type: string
        example: teeny tiny
  ClientInfo:
    type: object
    required:
      - client_id
      - client_token
    properties:
      client_id:
        type: string
        example: IAMNOTATEAPOT
      client_token:
        type: string
        example: RANDOMTOKENVALUE
  Token:
    type: object
    required:
      - token
    properties:
      token:
        type: string
        example: hXasdfawerASDFLJ34lskdfasjdfk
host: localhost
securityDefinitions:
  Bearer:
    type: apiKey
    name: Authorization
    in: header
basePath: /ehei/TrueToSizeApi/1.0.0
schemes:
  - https
