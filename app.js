const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const jwt = require('jsonwebtoken');
var debug = require('debug')('tts-node-api:server');

const knex = require('./db/knex');

const config = require('./config');

const index = require('./modules/status');
const datastore = require('./modules/datastore');
const auth = require('./modules/auth');
const middleware = require('./modules/middleware');


var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

debug('pre-knex');

knex.schema.hasTable('shoe').then(function(exists) {
    if (!exists) {
        debug("Table does not exist - run migrations and seed");
        knex.migrate.latest()
            .then(function() {
                debug("Migration run");
                knex.seed.run()
                    .then(function() {
                        debug("Seed run");
                    });
            })
    }
    else {
        debug("Table exists - just run migrations");
        knex.migrate.latest()
            .then(function() {
                debug("Migration run");
            });
    }
});

app.use(index());
app.use(auth(config));

app.use('/api', middleware.checkToken, datastore(knex));


module.exports = app;
