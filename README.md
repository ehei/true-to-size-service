# True to Size Service

## True to Size Service for StockX coding challenge.

[![pipeline status](https://gitlab.com/ehei/true-to-size-service/badges/master/pipeline.svg)](https://gitlab.com/ehei/true-to-size-service/commits/master)

### Tools / Technologies / Languages

- Webstorm
- NPM (6.1.0)
- NodeJS (6.11.3)
- Express
- PostgreSQL
- Knex
- Postman
- Newman
- Jest
- git
- GitLab
- Kubernetes

### Setup

* PostgreSQL setup
    * create two databases in PostgreSQL
        * truesizestore
        * truesizestore_test
    * create user 
        * `truesizeuser` 
        * set as owner of the databases 

Backups and script exports are also located under [/db/backup](./db/backup) and [/db/scripts](./db/scripts).

### Environment Variables

| Name | Value | |
| ------ | ------ | ----- |
| NODE_ENV | development / test | selects the appropriate knex config | 
| CLIENT_ID | < client id > | client id for auth |
| CLIENT_TOKEN | < client token > | client token for auth |
|debug | tts-node-api:server| if set, will provide more debug output to the console |

Postman collection uses: 

| Name | Value | 
| ------ | ------ |
|CLIENT_ID|api-client|
|CLIENT_TOKEN|not-so-test-token12312312|

I am leveraging the Webstorm project settings for dealing with the environment variables, all of which is currently ignored in git. There is a .idea folder zipped up into the project. Unzipping that into the cloned directory should allow a smoother experience if using IntelliJ or WebStorm. If not, then the relevant environment variable values for the npm scripts are located in the workspace.xml file.

### Testing

#### Unit Tests

The unit tests are alongside their respective test subjects, all located in the [modules] directory.
Test coverage is put out to the [coverage] directory.
They rely on the `NODE_ENV=test` environment variable for `knex`.

#### Acceptance / Integration Tests

The acceptance tests are written in Postman and can be imported into Postman and run or run with Newman.
There is also a version of the server, under `testbin/wwww` that will run the Newman test when the server starts up, and then stop the server.

#### Manual Testing

To do manual or exploratory testing, run the `start` script, then run the `resetDevData` script. This will start the server, and then put the development database into a good, known state with one entry in the shoe table with the data outlined in the Challenge document.

#### GitLab CI/CD

The project is on [GitLab](https://gitlab.com/ehei/true-to-size-service) although it is for obvious reasons currently private.
The API is available at [http://ehei-true-to-size-service.35.231.77.111.nip.io](http://ehei-true-to-size-service.35.231.77.111.nip.io).

Getting the CI/CD pipeline working necessitated some changes, particularly to the PostgreSQL environment variables, as well as adding some bootstrap code to the application to run the knex migrations and seed if needed.


### Thought process, Architecture and Design, and Things left by the wayside

As stated in the challenge, the goal was to create a NodeJS service that can accept data via HTTP and store in a relational database (PostgreSQL).
I started simply, getting a NodeJS+Express project going with some simple endpoints, test driven with Jest.
All the endpoints were initially unsecured, and I later added JWT using a secret key and an authentication endpoint that accepts a client id and client token.

To help drive the authentication piece as well as provide integration test coverage, I added Postman and Newman to the mix.

I attempted to keep the design as simple as possible. Some of the clean up tasks that I would tackle next would be pulling apart the datastore into a data service and a separate data store, to split up the responsibilities of data storage/retrieval and higher level data manipulation and representation.
This would also allow the mocking out of the database layer, to speed up unit tests and remove reliance of all but the data store tests on the backing database technology.

I would also look into other storage mediums, better ways to migrate the data and schema, and more ways to decouple of the storage mechanism from the tests and deployment. The database connections and separation into 'test' and 'development' were largely outgrown, so cleaning that up would also be worthwhile.


## Wrap up

Overall, this was a fun challenge, and definitely helps illuminate the sometimes overlooked effort and concerns with creating `a simple API`.
I think a perusal of the code, tests, and the git log should provide insight into how I go about crafting software solutions.

