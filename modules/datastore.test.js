const datastore = require('./datastore');
const knex = require('../db/knex');

const express = require('express');
const request = require('supertest');

const initIndex = () => {
    const app = express();
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.use(datastore(knex));
    return app;
};

describe('Routes: shoes', () => {
    beforeAll(() => {
        return knex.migrate.rollback()
            .then(() => knex.migrate.latest())
    });

    beforeEach(() => {
        return knex.seed.run()
    });

    afterEach(() => {
        return knex.table('shoe').truncate();
    });

    afterAll(() => {
        return knex.migrate.rollback()
            .then(knex.destroy());
    });

    var existingShoe = {
        name: 'first',
        size_calculation: '1.0000000000000',
        size_data: [1, 1, 1, 1]
    };

    var newShoe = {
        name: 'newShoe & Things >',
    };

    describe('GET /shoe', () => {
        test('Should return array of shoes', async () => {
            const app = initIndex();
            const res = await request(app).get('/shoe');
            expect(res.statusCode).toEqual(200);
            expect(res.body).toEqual({
                data: [
                    {
                        id: 1,
                        name: 'first',
                        size_calculation: '1.0000000000000',
                        size_data: [1, 1, 1, 1]
                    }
                 ]
            });
        });
    });

    describe('GET /shoe/:key ', () => {
        test('Id that does not exist will return 404 with no data', async () => {
            const app = initIndex();
            const res = await request(app).get('/shoe/90210');
            expect(res.statusCode).toEqual(404);
            expect(res.body).toEqual({
                error: "Shoe not found"
            });
        });
        test('Malformed Id will return 400 with no data', async () => {
            const app = initIndex();
            const res = await request(app).get('/shoe/XYZ');
            expect(res.statusCode).toEqual(400);
            expect(res.body).toEqual({
                error: "Invalid ID supplied"
            });
        });
        test('Id that does exist will return 200 with the data', async () => {
            const app = initIndex();
            const res = await request(app).get('/shoe/1');
            expect(res.statusCode).toEqual(200);
            expect(res.body).toEqual({
                id: 1,
                name: 'first',
                size_calculation: '1.0000000000000',
                size_data: [1, 1, 1, 1]
            });
        });
        test('Shoe name is unescaped on retrieval', async () => {
            const app = initIndex();
            const postRes = await request(app).post('/shoe').send(newShoe);
            const res = await request(app).get('/shoe/2');
            expect(res.statusCode).toEqual(200);
            expect(res.body.name).toEqual(newShoe.name);
        });
    });

    describe('POST /shoe/', () =>{

        test('Poorly formatted post should return 500', async () => {
            const app = initIndex();
            const res = await request(app).post('/shoe').send('');
            expect(res.statusCode).toEqual(500);
            expect(res.body).toEqual({
                error: "Error"
            });
        });
        test('Already existing named shoe will return 406', async () => {
            const app = initIndex();
            const res = await request(app).post('/shoe').send(existingShoe);
            expect(res.statusCode).toEqual(406);
            expect(res.body).toEqual({
                "error": "Key (name)=(first) already exists."
            });
        });
        test('New shoe will return 200 and the created ID', async () => {
            const app = initIndex();
            const res = await request(app).post('/shoe').send(newShoe);
            expect(res.statusCode).toEqual(200);
            expect(res.body).toEqual({
                data: {
                    id: 2
                }
            });
        });
        test('Special characters are escaped', async () => {
            var badShoeName = {
                name: 'bad shoe\'SELECT usename, usecreatedb, usesuper, usecatupd FROM pg_user; --\''
            };

            const app = initIndex();
            const res = await request(app).post('/shoe').send(badShoeName);
            expect(res.statusCode).toEqual(200);
            expect(res.body).toEqual({
                data: {
                    id: 2
                }
            });
        });
    });

    describe('POST /shoe/:key/', () =>{
        test('Poorly formatted post should return 500', async () => {
            const app = initIndex();
            const res = await request(app).post('/shoe/1').send({new_size: 'ick'});
            expect(res.statusCode).toEqual(500);
            expect(res.body).toEqual({
                error: "Size data must be a number from 1 to 5"
            });
        });
        test('New value will return 200 and the new size_calculation', async () => {
            const app = initIndex();
            const res = await request(app).post('/shoe/1').send({size: 5});
            expect(res.statusCode).toEqual(200);
            expect(res.body).toEqual({
                data: {
                    size_calculation: 1.8
                }
            });
        });
        test('New value will return 404 if the shoe id does not exist', async () => {
            const app = initIndex();
            const res = await request(app).post('/shoe/123').send({size: 5});
            expect(res.statusCode).toEqual(404);
            expect(res.body).toEqual({
                error: "Shoe not found"
            });
        });
        test('Trying to add a value other than 1-5 will return a 500', async () => {
            const app = initIndex();
            const res = await request(app).post('/shoe/1').send({size: 12});
            expect(res.statusCode).toEqual(500);
            expect(res.body).toEqual({
                error: "Size data must be a number from 1 to 5"
            });
        });

        test('Add size to new shoe initializes size array', async () => {
            const app = initIndex();
            const addRes = await request(app).post('/shoe').send(newShoe);
            expect(addRes.statusCode).toEqual(200);
            expect(addRes.body).toEqual({
                data: {
                    id: 2
                }
            });
            const res = await request(app).post('/shoe/2').send({size: 3});
            expect(res.statusCode).toEqual(200);
            expect(res.body).toEqual({
                data: {
                    size_calculation: 3
                }
            });
        });

    });

    describe('GET /truesize/:key ', () => {
        test('Id that does not exist will return 404 with no data', async () => {
            const app = initIndex();
            const res = await request(app).get('/truesize/90210');
            expect(res.statusCode).toEqual(404);
            expect(res.body).toEqual({
                "error": "Internal server error"
            });
        });
        test('Id that does exist will return 200 with the data', async () => {
            const app = initIndex();
            const res = await request(app).get('/truesize/1');
            expect(res.statusCode).toEqual(200);
            expect(res.body).toEqual({
                        truesize: 1,
                        description: 'really small'
            });
        });
    });

});