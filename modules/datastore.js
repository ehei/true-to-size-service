const {Router} = require('express');
const _ = require('lodash');
var debug = require('debug')('tts-node-api:server');

const defaultErrorCode = 500;
const defaultErrorMessage = 'Error';

const SIZES = [1,2,3,4,5];

const SIZE_DESCRIPTIONS = [
    { value: 1, desc: 'really small'},
    { value: 2, desc: 'small'},
    { value: 3, desc: 'true to size'},
    { value: 4, desc: 'big'},
    { value: 5, desc: 'really big'}
];

module.exports = (knexClient, router = new Router()) => {
    router.get('/shoe', async (req, res) => {

        debug('/shoe');
        var status = 500;
        var body = {
            error: 'Internal server error'
        };

        try {
            debug('query for shoes');
            const shoes = await knexClient.select().table('shoe');

            status = 200;
            body = {
                data: shoes
            };

        } catch (e) {
            debug('error');
            debug(e);
            status = e.statusCode || defaultErrorCode;
            body = {
                error: e.data || { message: defaultErrorMessage }
            };
        }

        return res.status(status).json(body);

    });

    router.get('/shoe/:key', async (req, res) => {
        const { key } = req.params;

        var status;
        var body;
        var goodKey = _.toSafeInteger(key);

        if (goodKey === 0) {
            status = 400;
            body = {
                error: 'Invalid ID supplied'
            };
        }
        else {

            try {
                const shoes = await knexClient.select().table('shoe').where('id', key)
                    .then(function (response) {

                        if (response.length === 0) {
                            status = 404;
                            body = {
                                error: "Shoe not found"
                            };
                        } else {
                            status = 200;
                            body = response[0];
                            body.name = _.unescape(body.name);
                        }
                    })
                    .catch(function (error) {
                        status = defaultErrorCode;
                        body = {
                            error: error
                        };
                    });

            } catch (e) {
                status = e.statusCode || 500;
                body = {
                    error: e.data || {message: defaultErrorMessage}
                };
            }
        }

        return res.status(_.defaultTo(status, 500)).json(_.defaultTo(body, {}));
    });

    router.post('/shoe', async (req, res) => {

        const { name } = req.body;

        var status = defaultErrorCode;
        var body = {
            error: defaultErrorMessage
        };

        if (! _.isNil(name) && !_.isEmpty(name)) {
            try {
                var escapedName = _.escape(name);
                var id = await knexClient.returning('id').insert({ name: escapedName }).table('shoe')
                    .then(function(ids) {

                        status = 200;
                        body = {
                            data: {
                                id: ids[0]
                            }
                        };
                    })
                    .catch(function(error) {
                        if (error.code === '23505') {
                            status = 406;

                            body = {
                                error: error.detail
                            }
                        }
                    });
            }
            catch (e) {
                status = e.statusCode || defaultErrorCode;
                body = {
                    error: e.data || { message: defaultErrorMessage }
                };
            }
        }

        return res.status(_.defaultTo(status, 500)).json(_.defaultTo(body, {}));
    });

    router.post('/shoe/:key', async (req, res) => {

        const { key } = req.params;
        const { size } = req.body;

        var status = 500;
        var body = {
            error: 'Internal server error'
        };

        if ( ! SIZES.includes(size)) {
            body.error = 'Size data must be a number from 1 to 5';
        }
        else {

            try {

                const shoe = await knexClient.select().table('shoe').where('id', key)
                    .then(function(response) {

                        if (response.length === 0) {
                            status = 404;
                            return null;
                        }
                        else {
                            status = 200;

                            return response[0];
                        }
                    })
                    .catch(function(error) {
                        status = defaultErrorCode;
                        body = {
                            error: error
                        };
                    });

                if (shoe !== null) {

                    if (shoe.size_data === null) {
                        shoe.size_data = [];
                    }
                    shoe.size_data.push(size);
                    let average = _.mean(shoe.size_data);
                    shoe.size_calculation = average;

                    var id = await knexClient.returning('size_calculation')
                        .table('shoe')
                        .update({
                            size_data: shoe.size_data,
                            size_calculation: average
                        })
                        .then(function (size_calculations) {
                            status = 200;

                            body = {
                                data: {
                                    size_calculation: average
                                }
                            };
                        })
                        .catch(function (error) {
                            status = 500;

                            body = {
                                error: defaultErrorMessage
                            }
                        });
                }
                else {
                    body = {
                        error: "Shoe not found"
                    };
                }
            }
            catch (e) {
                status = e.statusCode || defaultErrorCode;
                body = {
                    error: e.data || { message: defaultErrorMessage }
                };
            }
        }

        return res.status(_.defaultTo(status, 500)).json(_.defaultTo(body, {}));
    });

    router.get('/truesize/:key', async (req, res) => {
        const { key } = req.params;

        var status = 500;
        var body = {
            error: 'Internal server error'
        };

        try {
            const shoe = await knexClient.select().table('shoe').where('id', key)
                .then(function(response) {

                    if (response.length === 0) {
                        status = 404;
                        return null;
                    }
                    else {

                        status = 200;
                        return response[0];
                    }
                })
                .catch(function(error) {
                    status = defaultErrorCode;
                    body = {
                        error: error
                    };
                });

            if (shoe !== null) {

                var size_key = _.floor(shoe.size_calculation);
                var size_description = _.find(SIZE_DESCRIPTIONS, { value: size_key }).desc;

                body = {
                  truesize: _.toNumber(shoe.size_calculation),
                  description: size_description
                };
            }

        } catch (e) {
            status = e.statusCode || 500;
            body = {
                error: e.data || { message: defaultErrorMessage }
            };
        }

        return res.status(_.defaultTo(status, 500)).json(_.defaultTo(body, {}));
    });

    return router;
};
