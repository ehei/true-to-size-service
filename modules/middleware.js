const jwt = require('jsonwebtoken');
const config = require('../config');
const _ = require('lodash');
var debug = require('debug')('tts-node-api:server');

let checkToken = (req, res, next) => {

    debug('checking token');
    let token = req.headers['x-access-token'] || req.headers['authorization'] || req.headers['Authorization'];

    if (_.startsWith(token, 'Bearer ')) {

        debug('well formatted');
        token = token.slice(7, token.length);

        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Not a valid token'
                });
            }
            else {
                debug('verified');
                req.decoded = decoded;
                next();
            }
        });
    }
    else {
        debug('token error');
        return res.json({
            success: false,
            message: 'Missing token'
        });
    }
};

module.exports = {
    checkToken: checkToken
};