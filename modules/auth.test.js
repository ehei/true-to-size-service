const auth = require('./auth');
const express = require('express');
const request = require('supertest');
const jwt = require('jsonwebtoken');

const initAuth = () => {
    const app = express();
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));

    const config = {
        secret: '1234',
        app_id: 'test1',
        app_token: 'token1'
    };
    app.use(auth(config));
    return app;
};

describe('Routes: authenticate', () => {

    describe('POST /authenticate', () => {
        test('Should return 500 if post body is missing', async () => {
            const app = initAuth();
            const res = await request(app).post('/authenticate');
            expect(res.statusCode).toEqual(500);
        });
        test('Should return 500 if client id does not match', async () => {
            const app = initAuth();
            const res = await request(app).post('/authenticate').send({client_id: 'xyz'});
            expect(res.statusCode).toEqual(500);
        });
        test('Should return 500 if client token does not match', async () => {
            const app = initAuth();
            const res = await request(app).post('/authenticate').send({client_id: 'test1', client_token: 'qaa'});
            expect(res.statusCode).toEqual(500);
        });
        test('Should return 200 and the token if client id and token match', async () => {
            const app = initAuth();
            const res = await request(app).post('/authenticate').send({client_id: 'test1', client_token: 'token1'});
            expect(res.statusCode).toEqual(200);
            expect(res.body.token.length).toBeGreaterThan(10);
            const tokenPayload = jwt.decode(res.body.token, { complete: true }).payload;
            expect(tokenPayload).toHaveProperty('exp');

        });
    });
    describe('GET /authenticate', () => {
        test('Should return 404 if trying to GET', async () => {
            const app = initAuth();
            const res = await request(app).get('/authenticate');
            expect(res.statusCode).toEqual(404);
        });
    });
});