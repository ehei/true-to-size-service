const status = require('./status');

const express = require('express');
const request = require('supertest');

const initIndex = () => {
    const app = express();
    app.use(status());
    return app;
};

describe('General status endpoints', () => {
    test('Should return status of 200 OK', async() => {
        const app = initIndex();
        const res = await request(app).get('/ping');
        expect(res.statusCode).toEqual(200);
    });
    test('Should return status of UP', async() => {
        const app = initIndex();
        const res = await request(app).get('/status');
        expect(res.body).toEqual({
            status: 'UP'
        });
    });
});
