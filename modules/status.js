const {Router} = require('express');

module.exports = (router = new Router()) => {
  router.get('/status', async (req, res) => {
    return res.json({
      status: "UP"
    });
  });
  router.get('/ping', async (req, res) => {
    return res.status(200).end();
  });
  return router;
};