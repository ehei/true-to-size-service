const {Router} = require('express');
const _ = require('lodash');
const jwt = require('jsonwebtoken');

module.exports = (authConfig, router = new Router()) => {
    router.post('/authenticate', async (req, res) => {

        const { client_id, client_token } = req.body;

        if (_.isEqual(authConfig.app_id, client_id) && (_.isEqual(authConfig.app_token, client_token))) {

            const payload = {
                clientId: client_id,
                check:  true
            };

            const expiresIn24Hours = 1440;
            var token = jwt.sign(payload, authConfig.secret, {
                expiresIn: expiresIn24Hours
            });

            return res.json({
                token: token
            });
        }

        return res.status(500).end();
    });
    return router;
};