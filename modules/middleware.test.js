const middleware = require('./middleware');
const express = require('express');
const request = require('supertest');
const jwt = require('jsonwebtoken');
const httpMocks = require('node-mocks-http');
const _ = require('lodash');

const initMiddleware = () => {
    const app = express();
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));

    app.use('/', middleware.checkToken, async (req, res) => {

        if (_.isNil(req.decoded)) {
            return res.status(500).end();
        }
        else {
            return res.status(200).send(req.decoded);
        }
    });
    return app;
};

describe('checkToken', () => {

    test('Should reject with error if token is missing', async () => {

        const app = initMiddleware();

        const res = await request(app).get('/');
        expect(res.body).toEqual({
            success: false,
            message: 'Missing token'
        });
    });

    test('Should reject with error if token is not valid', async () => {

        const app = initMiddleware();

        const res = await request(app).get('/').set('Authorization', 'Bearer xzxc');
        expect(res.body).toEqual({
            success: false,
            message: 'Not a valid token'
        });

    });

    test('Should resolve if token is valid', async () => {

        const payload = {
            clientId: 'testclient',
            check:  true
        };

        const expiresIn24Hours = 1440;
        var token = jwt.sign(payload, 'SuperSecretSquirrel90210!!', {
            expiresIn: expiresIn24Hours
        });

        const app = initMiddleware();

        const res = await request(app).get('/').set('Authorization', 'Bearer ' + token);
        expect(res.statusCode).toEqual(200);
        expect(res.body.check).toEqual(true);
        expect(res.body.clientId).toEqual('testclient');
        expect(res.body.exp).toBeDefined();
        expect(res.body.iat).toBeDefined();

    });

});
