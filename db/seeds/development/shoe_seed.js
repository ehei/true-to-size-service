
exports.seed = function(knex, Promise) {
  return knex('shoe').del()
      .then(function () {
        return knex('shoe').insert({
          name: 'adidas Yeezy',
          size_data: [1,2,2,3,2,3,2,2,3,4,2,5,2,3],
          size_calculation: 2.5714285714286
        });
      });
};
