
exports.seed = function(knex, Promise) {
  return knex('shoe').del()
      .then(function () {
        return knex('shoe').insert({
          name: 'first',
          size_data: [1,1,1,1],
          size_calculation: 1
        });
      });
};
