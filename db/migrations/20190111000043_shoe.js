
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('shoe', function(table) {
      table.increments();
      table.string('name').notNullable().unique();
      table.specificType('size_data', 'integer ARRAY');
      table.decimal('size_calculation', 15, 13);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
      knex.schema.dropTableIfExists('shoe')
  ]);
};
