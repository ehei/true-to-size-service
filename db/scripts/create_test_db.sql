-- Database: truesizestore_test

-- DROP DATABASE truesizestore_test;

CREATE DATABASE truesizestore_test
    WITH
    OWNER = truesizeuser
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;